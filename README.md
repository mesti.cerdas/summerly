![](https://i.ibb.co/7KtDyPZ/logo.png)
#### <p align="center">**Summarize your review**</p>
---
<p align="center">
  <a href="#key-features">Key Features</a> •
  <a href="#how-to-use">How To Use</a> •
  <a href="#future-development">Future Development</a>
</p>

---
![](https://i.ibb.co/4NC3r5F/ex.png)

## Key Features

* Crawl as many as review you want
  - Just copy the URL, input the page and you will get the anime summary
* Customizable
* Offline use with database
* Simple algorithm, you can tweak it easily

![](https://i.ibb.co/XF3hJjS/Screenshot-2019-06-03-12-03-11.png)

## How to Use

To clone and run this application, you'll need [Git](https://git-scm.com) installed on your computer and of course a pip. From your command line:

```bash
# Install required packages
$ pip install nltk
$ pip install apyori
$ pip install selenium
$ pip install flask
$ pip install flask-bootstrap

# Clone the repository
$ git clone https://github.com/LoGic-b0ys/summerly

# Go into the repository
$ cd summary

# Execute it
$ python main.py
```

After that you can access the web from http://localhost:5000

## Future Development

Still Brainstorming

## Contribution

Contribute to this work by fork this repository. Don't forget to submit an issue first.

## Licence

GPL